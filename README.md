# pn-design-guidelines

## Icons

The icons have a filled appearance and an outlined appearance. The filled icon is our primary icon, but if the design demands an outlined icon, that is also permissable.

Icons in functional colours should follow the same rule as for functional text, and should only be used for their specific purpose.
Icons should primarly be used as free objects. On special occasions an icon can be placed in a circle or square, but this should be avoided if possible.

An outlined icon should ONLY be used when a filled icon takes to much visual space, and makes the design unbalanced. Filled icons and outlined icons should never be placed next to each other, or used together as an interactive effect.

### Icon colors

Solid: Digital Grey `#bebebe`

Outline: Digital Medium Grey `#757575`

![PostNord Icons](https://bitbucket.org/osserpse/pn-design-guidelines/raw/master/img/pn-icons.png) 
![PostNord Icons](https://bitbucket.org/osserpse/pn-design-guidelines/raw/master/img/pn-icons-b.png) 
![PostNord Icons](https://bitbucket.org/osserpse/pn-design-guidelines/raw/master/img/pn-icons-c.png) 
![PostNord Icons](https://bitbucket.org/osserpse/pn-design-guidelines/raw/master/img/pn-icons-d.png) 
![PostNord Icons](https://bitbucket.org/osserpse/pn-design-guidelines/raw/master/img/pn-icons-m.png) 
![PostNord Icons](https://bitbucket.org/osserpse/pn-design-guidelines/raw/master/img/pn-icons-p.png) 
![PostNord Icons](https://bitbucket.org/osserpse/pn-design-guidelines/raw/master/img/pn-icons-s.png) 
![PostNord Icons](https://bitbucket.org/osserpse/pn-design-guidelines/raw/master/img/pn-icons-t.png) 
![PostNord Icons](https://bitbucket.org/osserpse/pn-design-guidelines/raw/master/img/pn-icons-u.png) 
